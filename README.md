#### Fitmost test

##### Installing using Docker

Clone repository 

    git clone git@bitbucket.org:novikovd/fitmost-test.git

Start the docker

    docker-compose up -d

Run install script

    docker-compose run --rm backend sh install.sh

install.sh contains:

    # Install the application dependencies
    composer install
    
    # Initialize the application
    init --env=Development
        
    # Run migrations
    yes | php yii migrate
    yes | php yii migrate --migrationPath=@yii/rbac/migrations
    
    # Creates default roles and permissions
    php yii users/rbac/init
    
    # Creates default admin user (admin/admin)
    php yii users/user/create admin admin admin admin@test.com
    
    # Creates default operator user (operator/operator)
    php yii users/user/create operator operator operator operator@test.com
    
    # Creates default cities (Москва, Тула)
    php yii services/city/create-default
    
    
Access it in your browser by opening
    
    - frontend: http://127.0.0.1:20080
    - backend: http://127.0.0.1:21080
    
API requests examples:

    # Services list:
    http://127.0.0.1:20080/index.php?r=services/service/list
    
    # Services list by City name:
    http://127.0.0.1:20080/index.php?r=services/service/list&city_name=Москва
    
    # Services list by City ID:
    http://127.0.0.1:20080/index.php?r=services/service/list&city_id=2
    
    # Service by ID:
    http://127.0.0.1:20080/index.php?r=services/service/list&id=1
    http://127.0.0.1:20080/index.php?r=services/service/view&id=1
