<?php

use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

/* @var $this yii\web\View */
/* @var $content string */
?>

<?php $this->beginContent('@backend/views/layouts/main.php'); ?>

	<div class="row">

		<div class="col-xs-12">
            <?= Breadcrumbs::widget([
                'homeLink' => ['label' => 'Главная', 'url' => ['/']],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?= Alert::widget() ?>

            <?= $content ?>
		</div>

	</div>

<?php $this->endContent(); ?>