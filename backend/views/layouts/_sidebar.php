<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $content string */

?>

<?php Navbar::begin([
	'innerContainerOptions' => ['class' => 'container-fluid'],
	'containerOptions' => ['class' => 'collapse navbar-collapse'],
]);
	echo Nav::widget([
		'activateParents' => true,
		'options' => ['class' => ' nav  nav-stacked'],
		'items' => [
			[
				'label' => 'Услуги',
				'url' => ['/services/service/index'],
			],
            [
                'label' => 'Города',
                'url' => ['/services/city/index'],
            ],
            [
                'label' => 'История',
                'url' => ['/history/history/index'],
            ],
            [
                'label' => 'Пользователи',
                'url' => ['/users/user/index'],
            ],
		],

	]);

Navbar::end();
