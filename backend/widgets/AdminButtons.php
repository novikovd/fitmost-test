<?php

namespace backend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\ActiveRecord;

/**
 * Admin control buttons widget for CRUD pages
 */
class AdminButtons extends Widget
{
    /**
     * @var null|ActiveRecord model object
     */
	public $model;

    /**
     * @var string Action name (create, update, etc)
     */
	private $_action;

    /**
     * @inheritdoc
     */
	public function init()
	{
		$this->_action = Yii::$app->controller->action->id;
	}

    /**
     * @inheritdoc
     */
    public function run()
    {
        $return = Html::beginTag('div', ['class' => 'admin-btns']);

        foreach ($this->buttons() as $button) {
            $return .= ' ' . $this->button($button);
        }

        $return .= Html::endTag('div');

        return $return;
    }

    /**
     * @return array
     */
    private function buttons()
    {
        $buttons = [
            'index' => ['create'],
            'create' => ['index', 'save'],
            'update' => ['index', 'delete', 'create', 'save'],
        ];

        return $buttons[$this->_action] ?? [];
    }

    /**
     * @param string $name
     * @return string
     */
    private function button(string $name)
    {
        $method = $name . "Button";

        if (method_exists($this, $method)) {
            return $this->$method();
        }
    }

    /**
     * @return string
     */
    private function indexButton()
    {
        $return = Html::a('<i class="glyphicon glyphicon-th-list"></i> Список',
            ['index'],
            ['class' => 'btn btn-xs btn-info']
        );

        return $return;
    }

    /**
     * @return string
     */
    private function createButton()
    {
        $return = Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Создать',
            ['create'],
            ['class' => 'btn btn-xs btn-success']
        );

        return $return;
    }

    /**
     * @return string
     */
	private function deleteButton()
    {
        if (! $this->model) {
            return '';
        }

        return Html::a('<i class="glyphicon glyphicon-remove-sign"></i> Удалить',
            ['delete', 'id' => $this->model->id],
            [
                'class' => 'btn btn-xs btn-danger',
                'data-confirm' => 'Вы уверены?'
            ]
        );
    }

    /**
     * @return string
     */
    private function saveButton()
    {
        return Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Сохранить', [
            'class' => 'btn btn-xs btn-primary'
        ]);
    }

}