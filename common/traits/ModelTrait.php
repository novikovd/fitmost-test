<?php

namespace common\traits;

use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

trait ModelTrait
{
    /**
     * @param string $class
     * @param integer $id
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public function findModel(string $class, int $id)
    {
        /* @var ActiveRecord $class */
        $model = $class::findOne(['id' => $id]);

        if (! $model) {
            throw new NotFoundHttpException('Model not found');
        }

        return $model;
    }

}
