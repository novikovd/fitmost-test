<?php

namespace frontend\base;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;

class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
	public function behaviors()
	{
		return array_merge(parent::behaviors(), [
            'verbFilter' => [
                'class' => VerbFilter::class,
                'actions' => [
                    '*' => ['GET', 'POST'],
                ],
            ],
//            'authenticator' => [
//                'class' => HttpBearerAuth::class,
//            ],
//			'access' => [
//				'class' => AccessControl::class,
//				'rules' =>  [
//					[
//						'allow' => true,
//						'roles' => ['@']
//					],
//				]
//			],
		]);
	}

}