<?php
namespace modules\users\backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\traits\ModelTrait;
use backend\base\Controller;
use modules\users\backend\models\UserSearch;
use modules\users\backend\models\User;

class UserController extends Controller
{
    use ModelTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => $this->getGridColumns(),
        ]);
    }

    /**
     * @return array
     */
    public function getGridColumns()
    {
        return [
            'id',
            'username',
            [
                'attribute' => 'role',
                'filter' => User::rolesList(),
                'value' => function (UserSearch $model) {
                    return User::rolesList()[$model->authAssignment->item_name] ?? null;
                }
            ],
            'email',
        ];
    }

}