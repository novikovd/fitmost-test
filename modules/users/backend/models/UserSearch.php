<?php
namespace modules\users\backend\models;

use yii\db\ActiveQuery;
use backend\search\SearchModelInterface;
use backend\search\SearchModelTrait;

class UserSearch extends User implements SearchModelInterface
{
    use SearchModelTrait;

    /**
     * @var string
     */
    public $role;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],

            [['username', 'role', 'email'], 'safe'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = static::find();

        $query->joinWith([
            'authAssignment',
        ]);

        return $query;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            '{{%user}}.id' => $this->id,
            '{{%auth_assignment}}.item_name' => $this->role,
        ]);

        $query->andFilterWhere(['LIKE', '{{%user}}.username', $this->username]);
        $query->andFilterWhere(['LIKE', '{{%user}}.email', $this->email]);
    }
}
