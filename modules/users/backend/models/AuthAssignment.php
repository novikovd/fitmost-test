<?php
namespace modules\users\backend\models;

use yii\db\ActiveRecord;

/**
 * @property string $item_name
 * @property integer $user_id
 * @property integer $created_at
 */
class AuthAssignment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%auth_assignment}}';
    }
}
