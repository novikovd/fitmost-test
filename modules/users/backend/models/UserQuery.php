<?php

namespace modules\users\backend\models;

use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{
    /**
     * @param string $username
     * @return $this
     */
    public function byUsername(string $username)
    {
        $this->andWhere(['username' => $username]);

        return $this;
    }
}
