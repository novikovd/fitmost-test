<?php

/**
 * @var yii\web\View $this
 * @var modules\users\backend\models\UserSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var array $columns
 */

use yii\grid\GridView;
use yii\grid\ActionColumn;
use backend\widgets\AdminButtons;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<?= AdminButtons::widget() ?>

<?= GridView::widget([
    'id' => 'grid-users',
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => array_merge(
        $columns, [
        [
			'class' => ActionColumn::class,
			'template' => '{update} {delete}',
        ]
    ]),
]); ?>
