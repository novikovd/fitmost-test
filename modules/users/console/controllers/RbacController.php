<?php

namespace modules\users\console\controllers;

use Yii;
use yii\base\Exception;
use yii\console\Controller;


class RbacController extends Controller
{
    /**
     * Create default roles and permissions
     * @throws Exception
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        // Roles
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';

        $operator = $auth->createRole('operator');
        $operator->description = 'Оператор';

        $auth->add($admin);
        $auth->add($operator);

        // Permissions
        $serviceStatusUpdate = $auth->createPermission('serviceStatusUpdate');
        $serviceStatusUpdate->description = 'Редактирование статуса услуги';

        $auth->add($serviceStatusUpdate);

        // Permissions to roles
        $auth->addChild($admin, $serviceStatusUpdate);

    }
}
