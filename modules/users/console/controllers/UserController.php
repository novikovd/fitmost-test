<?php

namespace modules\users\console\controllers;

use Yii;
use yii\helpers\Console;
use yii\console\ExitCode;
use yii\console\Controller;
use yii\base\Exception;
use modules\users\backend\models\User;

class UserController extends Controller
{
    /**
     * Creates admin user
     *
     * @param string $role
     * @param string $username
     * @param string $password
     * @param string $email
     * @return int
     * @throws Exception
     */
    public function actionCreate(string $role, string $username, string $password, string $email)
    {
        $user = User::find()
            ->byUsername($username)
            ->one();

        if (! isset(User::rolesList()[$role])) {
            Console::output(Console::ansiFormat("Role doesn't exists", [Console::FG_YELLOW]));

            return ExitCode::UNSPECIFIED_ERROR;
        }

        if ($user) {
            Console::output(Console::ansiFormat("Username already exists", [Console::FG_YELLOW]));

            return ExitCode::OK;
        }

        $model = new User;
        $model->username = $username;
        $model->email = $email;
        $model->status = User::STATUS_ACTIVE;
        $model->setPassword($password);
        $model->generateAuthKey();

        if (! $model->save()) {
            Console::output(
                Console::ansiFormat(
                    "Failed to create user: "  . implode(', ', $model->getFirstErrors()),
                    [Console::FG_RED]
                )
            );

            return ExitCode::UNSPECIFIED_ERROR;
        }

        $auth = Yii::$app->authManager;
        $authRole = $auth->getRole($role);
        $auth->assign($authRole, $model->id);

        Console::output(Console::ansiFormat("User created successfully", [Console::FG_GREEN]));

        return ExitCode::OK;
    }
}
