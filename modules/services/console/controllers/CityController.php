<?php

namespace modules\services\console\controllers;

use yii\helpers\Console;
use yii\console\ExitCode;
use yii\console\Controller;
use modules\services\common\models\City;

class CityController extends Controller
{
    /**
     * Creates default cities
     *
     * @return int
     */
    public function actionCreateDefault()
    {
        $cities = ['Москва', 'Тула'];

        foreach ($cities as $city) {
            $model = new City;
            $model->title = $city;

            if (! $model->save()) {
                Console::output(
                    Console::ansiFormat(
                        "Failed to create city: "  . implode(', ', $model->getFirstErrors()),
                        [Console::FG_RED]
                    )
                );

                return ExitCode::UNSPECIFIED_ERROR;
            }
        }

        Console::output(Console::ansiFormat("Default cities created successfully", [Console::FG_GREEN]));

        return ExitCode::OK;
    }
}
