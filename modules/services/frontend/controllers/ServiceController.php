<?php

namespace modules\services\frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use frontend\base\ApiController;
use modules\services\frontend\models\ServiceListSearch;
use modules\services\common\models\Service;

class ServiceController extends ApiController
{
    /**
     * @return ActiveDataProvider
     */
    public function actionList()
    {
        $searchModel = new ServiceListSearch();
        $dataProvider = $searchModel->search(['ServiceListSearch' => Yii::$app->request->queryParams]);

        return $dataProvider;
    }

    /**
     * @param int $id
     * @return Service|null
     */
    public function actionView(int $id)
    {
        $model = Service::findOne(['id' => $id]);

        return $model;
    }

}
