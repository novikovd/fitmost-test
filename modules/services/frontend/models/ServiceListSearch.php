<?php

namespace modules\services\frontend\models;

use yii\db\ActiveQuery;
use backend\search\SearchModelInterface;
use backend\search\SearchModelTrait;
use modules\services\common\models\Service;

class ServiceListSearch extends Service implements SearchModelInterface
{
    use SearchModelTrait;

    /**
     * @var string
     */
    public $city_name;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'city_id'], 'integer'],

            [['city_name'], 'safe'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = static::find();

        $query->joinWith(['city']);

        return $query;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            '{{%service}}.id' => $this->id,
            '{{%service}}.city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['LIKE', '{{%city}}.title', $this->city_name]);
    }

}