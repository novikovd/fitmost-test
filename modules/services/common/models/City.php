<?php
namespace modules\services\common\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\traits\ModelListTrait;

/**
 * @property integer $id
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 */
class City extends ActiveRecord
{
    use ModelListTrait;

    /**
     * @inheritdoc
     */
	public static function tableName()
	{
		return '{{%city}}';
	}

    /**
     * @inheritdoc
     */
	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
			['title', 'filter', 'filter' => 'trim'],
			['title', 'required'],
			['title', 'string', 'max' => 255],
            ['title', 'unique'],

            [['created_at', 'updated_at'], 'safe'],
		];
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'title',
        ];
    }
}