<?php
namespace modules\services\common\models;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use modules\history\common\behaviors\HistoryBehavior;

/**
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $price
 * @property string $description
 * @property integer $status
 * @property integer $city_id
 * @property string $expiry_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property City $city
 */
class Service extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;

    /**
     * @inheritdoc
     */
	public static function tableName()
	{
		return '{{%service}}';
	}

    /**
     * @return array
     */
    public static function statusesList()
    {
        return [
            self::STATUS_ACTIVE => 'Включен',
            self::STATUS_DISABLED => 'Отключен',
        ];
    }

    /**
     * @inheritdoc
     */
	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'code' => 'Код',
            'price' => 'Цена',
            'description' => 'Описание',
            'status' => 'Статус',
            'city_id' => 'Город действия',
            'expiry_at' => 'Срок действия',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
			['title', 'filter', 'filter' => 'trim'],
			['title', 'required'],
			['title', 'string', 'max' => 255],

            ['code', 'filter', 'filter' => 'trim'],
            ['code', 'required'],
            ['code', 'string', 'max' => 255],
            ['code', 'unique'],

            ['price', 'required'],
            ['price', 'integer', 'min' => 1],

            ['description', 'safe'],

            ['status', 'in', 'range' => array_keys(self::statusesList())],
            ['status', 'default', 'value' => self::STATUS_DISABLED],

            ['city_id', 'required'],
            ['city_id', 'integer'],

            ['expiry_at', 'required'],
            ['expiry_at', 'date', 'format' => 'php:Y-m-d H:i:s'],

            [['created_at', 'updated_at'], 'safe'],
		];
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'History' => [
                'class' => HistoryBehavior::class,
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'title',
            'code',
            'description',
            'status',
            'expiry_at',
            'city',
        ];
    }
}