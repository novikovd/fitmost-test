<?php

use yii\db\Migration;

/**
 * Class m190518_184112_services_create_table_service
 */
class m190518_184112_services_create_table_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'code' => $this->string()->notNull()->unique(),
            'price' => $this->integer()->notNull(),
            'description' => $this->text(),
            'status' => $this->boolean(),
            'city_id' => $this->integer()->notNull(),
            'expiry_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->addForeignKey('{{%fk-service-city}}',
            '{{%service}}', 'city_id',
            '{{%city}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-service-city}}', '{{%service}}');

        $this->dropTable('{{%service}}');
    }

}
