<?php

/**
 * @var yii\web\View $this
 * @var modules\services\common\models\City $model
 */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
]); ?>
