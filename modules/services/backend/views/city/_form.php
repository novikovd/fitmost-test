<?php

/**
 * @var View $this
 * @var City $model
 * @var ActiveForm $form
 */

use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\widgets\AdminButtons;
use modules\services\common\models\City;

?>

<?php $form = ActiveForm::begin(); ?>

    <?= AdminButtons::widget(['model' => $model]); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>
