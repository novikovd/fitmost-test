<?php

/**
 * @var View $this
 * @var CitySearch $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var array $columns
 */

use yii\web\View;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\data\ActiveDataProvider;
use backend\widgets\AdminButtons;
use modules\services\backend\models\CitySearch;

$this->title = 'Города';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<?= AdminButtons::widget() ?>

<?= GridView::widget([
    'id' => 'grid-cities',
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => array_merge(
        $columns, [
        [
			'class' => ActionColumn::class,
			'template' => '{update} {delete}',
        ]
    ]),
]); ?>
