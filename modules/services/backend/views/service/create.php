<?php

/**
 * @var yii\web\View $this
 * @var modules\services\common\models\Service $model
 * @var modules\users\backend\models\User $user
 * @var array $cities
 * @var array $statuses
 */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'user' => $user,
    'cities' => $cities,
    'statuses' => $statuses,
]); ?>
