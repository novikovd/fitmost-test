<?php

/**
 * @var yii\web\View $this
 * @var ActiveForm $form
 * @var modules\services\common\models\Service $model
 * @var modules\users\backend\models\User $user
 * @var array $statuses
 * @var array $cities
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datetime\DateTimePicker;
use backend\widgets\AdminButtons;

?>

<?php $form = ActiveForm::begin(); ?>

    <?= AdminButtons::widget(['model' => $model]); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'code')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'status')->dropDownList($statuses, [
        'disabled' => ! $user->can('serviceStatusUpdate')
    ]) ?>

    <?php echo $form->field($model, 'expiry_at')->widget(DateTimePicker::class, [
        'options' => [
            'placeholder' => 'Выбрать ...',
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii:ss',
        ]
    ]) ?>

    <?= $form->field($model, 'city_id')->dropDownList($cities, ['prompt' => 'Выбрать...']) ?>

    <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>
