<?php
namespace modules\services\backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use common\traits\ModelTrait;
use backend\base\Controller;
use modules\services\common\models\Service;
use modules\services\backend\models\ServiceSearch;
use modules\services\common\models\City;

class ServiceController extends Controller
{
    use ModelTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'update'],
                        'roles' => ['operator'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => $this->getGridColumns(),
        ]);
    }

    /**
     * @return array
     */
    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(ServiceSearch $model) {
                    return Html::a($model->title, ['update', 'id' => $model->id]);
                }
            ],
            'code',
            'price',
            [
                'attribute' => 'status',
                'filter' => Service::statusesList(),
                'value' => function(ServiceSearch $model) {
                    return Service::statusesList()[$model->status] ?? null;
                }
            ],
            'expiry_at',
            [
                'attribute' => 'city_id',
                'filter' => City::getList(),
                'value' => function(ServiceSearch $model) {
                    return $model->city->title ?? null;
                }
            ],
            [
                'label' => 'История изменений',
                'format' => 'html',
                'value' => function(ServiceSearch $model) {
                    return Html::a('История', [
                        '/history/history/view',
                        'model_id' => $model->id,
                        'model_class' => Service::class,
                    ]);
                }
            ],
        ];
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Service();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Услуга успешно создана.');

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'user' => Yii::$app->user,
            'cities' => City::getList(),
            'statuses' => Service::statusesList(),
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var Service $model */
        $model = $this->findModel(Service::class, $id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Услуга успешно обновлена.');

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'user' => Yii::$app->user,
            'cities' => City::getList(),
            'statuses' => Service::statusesList(),
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException|\Throwable|\yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel(Service::class, $id)->delete();

        Yii::$app->session->setFlash('success', 'Услуга успешно удалена.');

        return $this->redirect(['index']);
    }

}
