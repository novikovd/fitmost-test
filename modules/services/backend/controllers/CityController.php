<?php
namespace modules\services\backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use common\traits\ModelTrait;
use backend\base\Controller;
use modules\services\common\models\City;
use modules\services\backend\models\CitySearch;

class CityController extends Controller
{
    use ModelTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => $this->getGridColumns(),
        ]);
    }

    /**
     * @return array
     */
    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function(CitySearch $model) {
                    return Html::a($model->title, ['update', 'id' => $model->id]);
                }
            ],
        ];
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new City();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Город успешно создан.');

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var City $model */
        $model = $this->findModel(City::class, $id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Город успешно обновлен.');

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException|\Throwable|\yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel(City::class, $id)->delete();

        Yii::$app->session->setFlash('success', 'Город успешно удален.');

        return $this->redirect(['index']);
    }

}
