<?php
namespace modules\services\backend\models;

use yii\db\ActiveQuery;
use backend\search\SearchModelInterface;
use backend\search\SearchModelTrait;
use modules\services\common\models\City;

class CitySearch extends City implements SearchModelInterface
{
    use SearchModelTrait;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],

            [['title'], 'safe'],
        ];
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            '{{%city}}.id' => $this->id,
        ]);

        $query->andFilterWhere(['LIKE', '{{%city}}.title', $this->title]);
    }
}
