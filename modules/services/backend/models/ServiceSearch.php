<?php
namespace modules\services\backend\models;

use yii\db\ActiveQuery;
use backend\search\SearchModelInterface;
use backend\search\SearchModelTrait;
use modules\services\common\models\Service;

class ServiceSearch extends Service implements SearchModelInterface
{
    use SearchModelTrait;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'price', 'city_id', 'status'], 'integer'],

            [['title', 'code', 'description'], 'safe'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = static::find();

        $query->joinWith(['city']);

        return $query;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            '{{%service}}.id' => $this->id,
            '{{%service}}.price' => $this->price,
            '{{%service}}.city_id' => $this->city_id,
            '{{%service}}.status' => $this->status,
        ]);

        $query->andFilterWhere(['LIKE', '{{%service}}.title', $this->title]);
        $query->andFilterWhere(['LIKE', '{{%service}}.code', $this->code]);
        $query->andFilterWhere(['LIKE', '{{%service}}.description', $this->description]);
    }
}
