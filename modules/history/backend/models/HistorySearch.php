<?php
namespace modules\history\backend\models;

use yii\db\ActiveQuery;
use yii\data\ActiveDataProvider;
use backend\search\SearchModelInterface;
use backend\search\SearchModelTrait;
use modules\history\common\models\History;

class HistorySearch extends History implements SearchModelInterface
{
    use SearchModelTrait;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = static::find();

        $query->joinWith([
            'user',
            'logs',
        ]);

        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            '{{%history}}.id' => $this->id,
        ]);
    }
}
