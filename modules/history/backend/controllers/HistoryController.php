<?php
namespace modules\history\backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\StringHelper;
use common\traits\ModelTrait;
use backend\base\Controller;
use modules\history\backend\models\HistorySearch;
use modules\history\common\models\History;

class HistoryController extends Controller
{
    use ModelTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['admin', 'operator'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => $this->getGridColumns(),
        ]);
    }

    /**
     * @return array
     */
    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'model_class',
                'value' => function (HistorySearch $model) {
                    return StringHelper::basename($model->model_class);
                }
            ],
            'model_id',
            [
                'attribute' => 'user_id',
                'value' => function (HistorySearch $model) {
                    return $model->user->username ?? null;
                }
            ],
            'action',
            [
                'label' => 'История',
                'format' => 'html',
                'value' => function (HistorySearch $model) {
                    return $this->renderPartial('_logs', [
                        'model' => $model,
                    ]);
                }
            ]
        ];
    }

    /**
     * @param int $model_id
     * @param string $model_class
     * @return string
     */
    public function actionView(int $model_id, string $model_class)
    {
        $models = History::find()
            ->joinWith([
                'user',
                'logs'
            ])
            ->where([
                '{{%history}}.model_id' => $model_id,
                '{{%history}}.model_class' => $model_class
            ])
            ->orderBy(['{{%history}}.created_at' => SORT_DESC])
            ->all();

        return $this->render('view', [
            'models' => $models
        ]);
    }

}