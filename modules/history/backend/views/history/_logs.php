<?php

/**
 * @var yii\web\View $this
 * @var History $model
 */

use modules\history\common\models\History;

?>

<?php if (History::ACTION_CREATE === $model->action) : ?>
    <span class="label label-success"><?= History::actionsList()[History::ACTION_CREATE] ?></span>
<?php endif;?>

<?php if (History::ACTION_DELETE === $model->action) : ?>
    <span class="label label-warning"><?= History::actionsList()[History::ACTION_DELETE] ?></span>
<?php endif;?>

<?php if ($model->logs) : ?>

    <?php foreach ($model->logs as $log) : ?>

        <?php if (History::ACTION_UPDATE === $log->action) : ?>

            <div>
                <span class="label label-info"><?= History::actionsList()[History::ACTION_UPDATE] ?></span>
                поле <b><?php echo $log->attribute ?></b>
            </div>

            <br>

            <table class="table small">
                <tr>
                    <td>Старое</td>
                    <td>Новое</td>
                </tr>
                <tr>
                    <td><?= $log->old_value; ?></td>
                    <td><?= $log->new_value; ?></td>
                </tr>
            </table>

        <?php endif;?>

    <?php endforeach; ?>

<?php endif; ?>