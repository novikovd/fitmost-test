<?php

/**
 * @var View $this
 * @var HistorySearch $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var array $columns
 */

use yii\web\View;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use modules\history\backend\models\HistorySearch;

$this->title = 'История';
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<?= GridView::widget([
    'id' => 'grid-history',
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $columns,
]); ?>
