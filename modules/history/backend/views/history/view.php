<?php

/**
 * @var yii\web\View $this
 * @var modules\history\common\models\History[] $models
 */

$this->title = 'История';
$this->params['breadcrumbs'][] = ['label' => 'История', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>


<table class="table">
    <tr>
        <th>Дата</th>
        <th>Пользователь</th>
        <th>Действие</th>
        <th>История</th>
    </tr>
    <?php foreach ($models as $model) :  ?>
        <tr>
            <td><?= $model->created_at ?></td>
            <td><?= $model->user->username ?></td>
            <td><?= $model->action ?></td>
            <td>
                <?= $this->render('_logs', ['model' => $model]); ?>
            </td>
        </tr>
    <?php endforeach; ?>

</table>
