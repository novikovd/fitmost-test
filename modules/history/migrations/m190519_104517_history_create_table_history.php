<?php

use yii\db\Migration;

/**
 * Class m190519_104517_history_create_table_history
 */
class m190519_104517_history_create_table_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%history}}', [
            'id' => $this->primaryKey(),
            'model_class' => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'action' =>  $this->string(16)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createIndex('model_id', '{{%history}}', 'model_id');
        $this->createIndex('user_id', '{{%history}}', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%history}}');
    }

}
