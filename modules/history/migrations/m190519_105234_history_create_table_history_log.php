<?php

use yii\db\Migration;

/**
 * Class m190519_105234_history_create_table_history_log
 */
class m190519_105234_history_create_table_history_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%history_log}}', [
            'id' => $this->primaryKey(),
            'history_id' => $this->integer()->notNull(),
            'model_class' => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'attribute' =>  $this->string(32),
            'old_value' =>  $this->string(),
            'new_value' =>  $this->string(),
            'action' =>  $this->string(16)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createIndex('model_id', '{{%history_log}}', 'model_id');

        $this->addForeignKey('{{%fk-history_log-history}}',
            '{{%history_log}}', 'history_id',
            '{{%history}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-history_log-history}}', '{{%history_log}}');

        $this->dropTable('{{%history_log}}');
    }

}
