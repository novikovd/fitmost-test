<?php
namespace modules\history\common\models;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $history_id
 * @property string $model_class
 * @property integer $model_id
 * @property string $attribute
 * @property string $old_value
 * @property string $new_value
 * @property string $action
 * @property string $created_at
 * @property string $updated_at
 */
class HistoryLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%history_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['history_id', 'required'],
            ['history_id', 'integer'],

            ['model_class', 'required'],
            ['model_class', 'string', 'max' => 255],

            ['model_id', 'required'],
            ['model_id', 'integer'],

            ['attribute', 'string', 'max' => 32],

            ['old_value', 'string', 'max' => 255],

            ['new_value', 'string', 'max' => 255],

            ['action', 'required'],
            ['action', 'string', 'max' => 16],
            ['action', 'in', 'range' => array_keys(History::actionsList())],

            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'history_id' => 'History ID',
            'model_class' => 'Model Class',
            'model_id' => 'Model ID',
            'attribute' => 'Атрибут',
            'old_value' => 'Старое значение',
            'new_value' => 'Новое значение',
            'action' => 'Действие',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

}
