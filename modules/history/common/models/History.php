<?php
namespace modules\history\common\models;

use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use modules\users\backend\models\User;

/**
 * @property integer $id
 * @property string $model_class
 * @property integer $model_id
 * @property integer $user_id
 * @property string $action
 * @property integer $is_manager_viewed
 * @property string $created_at
 * @property string $updated_at
 *
 * @property HistoryLog[] $logs
 * @property User $user
 */
class History extends ActiveRecord
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    const ACTION_ADD = 'add';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%history}}';
    }

    /**
     * @return array
     */
    public static function actionsList()
    {
        return [
            self::ACTION_CREATE => 'Создание',
            self::ACTION_UPDATE => 'Обновление',
            self::ACTION_DELETE => 'Удаление',
            self::ACTION_ADD => 'Добавление',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['model_class', 'required'],
            ['model_class', 'string', 'max' => 255],

            ['model_id', 'required'],
            ['model_id', 'integer'],

            ['user_id', 'required'],
            ['user_id', 'integer'],

            ['action', 'required'],
            ['action', 'string', 'max' => 16],
            ['action', 'in', 'range' => array_keys(self::actionsList())],

            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_class' => 'Model Class',
            'model_id' => 'Model ID',
            'user_id' => 'Автор',
            'action' => 'Действие',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(HistoryLog::class, ['history_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
