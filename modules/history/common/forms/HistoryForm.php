<?php
namespace modules\history\common\forms;

use yii\base\Exception;
use modules\history\common\models\History;
use modules\history\common\models\HistoryLog;

class HistoryForm extends History
{
    /**
     * @var HistoryLog[]
     */
    private $_logs;

    /**
     * @param HistoryLog $log
     * @return void
     */
    public function addLog(HistoryLog $log)
    {
        $this->_logs[] = $log;
    }

    /**
     * @return bool
     */
    public function saveAll()
    {
        $transaction = self::getDb()->beginTransaction();

        try {
            if (! $this->save()) {
                throw new Exception('Failed to save history: ' . implode(', ', $this->getFirstErrors()));
            }

            $this->saveLogs();

            $transaction->commit();

            return true;

        } catch (Exception $e) {
            $transaction->rollBack();

            return false;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function saveLogs()
    {
        if (! $this->_logs) {
            return false;
        }

        foreach ($this->_logs as $log) {
            $log->history_id = $this->id;
            $log->action = $log->action ? $log->action : $this->action;

            if (! $log->save()) {
                throw new Exception('Failed to save history log: ' . implode(', ', $log->getFirstErrors()));
            }
        }

        return true;
    }

}
