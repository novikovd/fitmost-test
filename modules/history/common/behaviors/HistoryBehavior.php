<?php
namespace modules\history\common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use modules\users\backend\models\User;
use modules\history\common\models\History;
use modules\history\common\models\HistoryLog;
use modules\history\common\forms\HistoryForm;

class HistoryBehavior extends Behavior
{
    /**
     * @var array Exceptions attributes array
     */
    public $exceptionAttributes = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array HasMany relations models options
     *
     * Example:
     *
     * ```php
     * 'History' => [
     *     'class' => HistoryBehavior::class,
     *     'relations' => [
     *         'city' => [
     *             'class' => City::class,
     *             'attribute' => 'cities',
     *             'field' => 'city_id'
     *         ]
     *     ],
     * ]
     * ```
     */
    public $relations;

    /**
     * @var User Current user model
     */
    private $_user;

    /**
     * @var array After find event model state
     */
    private $_oldAttributes;

    /**
     * @inheritdoc
     */
    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->_user = Yii::$app->user;
    }

    /**
     * @return void
     */
    public function afterFind()
    {
        /* @var ActiveRecord $owner */
        $owner = $this->owner;
        $attributes = $owner->safeAttributes();

        foreach ($attributes as $attribute) {
            if ($this->relations && isset($this->relations[$attribute])) {
                $relationAttribute = $this->relations[$attribute]['attribute'];
                $relations = $this->owner->$relationAttribute;
                $this->_oldAttributes[$attribute] = ArrayHelper::getColumn($relations, 'id');
            } else {
                $this->_oldAttributes[$attribute] = $this->owner->$attribute;
            }
        }
    }

    /**
     * @return bool
     */
    public function afterInsert()
    {
        $history = $this->createHistory();

        foreach ($this->owner->safeAttributes() as $attribute) {
            if (in_array($attribute, $this->exceptionAttributes)) {
                continue;
            }

            $value = $this->owner->$attribute;

            if (isset($this->relations[$attribute]) && is_array($value) ) {
                $relation = $this->relations[$attribute];

                foreach ($value as $newValue) {
                    $log = new HistoryLog;
                    $log->action = History::ACTION_CREATE;
                    $log->model_class = $relation['class'];
                    $log->model_id = $newValue;
                    $log->attribute = $relation['field'];
                    $log->new_value = (string) $newValue;
                    $history->addLog($log);
                }
            }
        }

        return $history->saveAll();
    }

    /**
     * @return bool
     */
    public function afterDelete()
    {
        $history = $this->createHistory(History::ACTION_DELETE);

        foreach ($this->_oldAttributes as $attribute => $values) {
            if (in_array($attribute, $this->exceptionAttributes)) {
                continue;
            }

            if (isset($this->relations[$attribute]) && is_array($values) ) {
                $relation = $this->relations[$attribute];

                foreach ($values as $oldValue) {
                    $log = new HistoryLog;
                    $log->action = History::ACTION_DELETE;
                    $log->model_class = $relation['class'];
                    $log->model_id = $oldValue;
                    $log->attribute = $relation['field'];
                    $log->old_value = (string) $oldValue;
                    $history->addLog($log);
                }
            }
        }

        return $history->saveAll();
    }

    /**
     * @return bool
     */
    public function afterUpdate()
    {
        $history = $this->createHistory(History::ACTION_UPDATE);

        foreach ($this->_oldAttributes as $attribute => $value) {
            if (in_array($attribute, $this->exceptionAttributes)) {
                continue;
            }

            $oldValue = ArrayHelper::getValue($this->_oldAttributes, $attribute);
            $newValue = $this->owner->$attribute;

            if (is_string($newValue) && ! $this->owner->isNewRecord) {
                if ($oldValue != $newValue) {
                    $log = new HistoryLog;
                    $log->action = History::ACTION_UPDATE;
                    $log->model_class = $this->owner->className();
                    $log->model_id = $this->owner->id;
                    $log->attribute = $attribute;
                    $log->old_value = (string)$oldValue;
                    $log->new_value = (string)$newValue;
                    $history->addLog($log);
                }
            }

            if (is_array($newValue) && isset($this->relations[$attribute])) {
                $relation = $this->relations[$attribute];
                $oldValues = $oldValue;
                $newValues = array_map('intval', $newValue);

                foreach ($oldValues as $oldModeId) {
                    if (! in_array($oldModeId, $newValues)) {
                        $log = new HistoryLog;
                        $log->action = History::ACTION_DELETE;
                        $log->model_class = $relation['class'];
                        $log->model_id = $oldModeId;
                        $log->attribute = $relation['field'];
                        $log->old_value = (string) $oldModeId;
                        $history->addLog($log);
                    }
                }

                if ($newValues) {
                    foreach ($newValues as $newModeId) {
                        if (! in_array($newModeId, $oldValues)) {
                            $log = new HistoryLog;
                            $log->action = History::ACTION_CREATE;
                            $log->model_class = $relation['class'];
                            $log->model_id = $newModeId;
                            $log->attribute = $relation['field'];
                            $log->new_value = (string) $newModeId;
                            $history->addLog($log);
                        }
                    }
                }
            }
        }

        return $history->saveAll();
    }

    /**
     * @param string $action
     * @return HistoryForm
     */
    private function createHistory(string $action = History::ACTION_CREATE)
    {
        $history = new HistoryForm();
        $history->model_class = $this->owner->className();
        $history->model_id = $this->owner->id;
        $history->user_id = $this->_user->getId();
        $history->action = $action;

        return $history;
    }

}
