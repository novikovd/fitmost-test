#!/usr/bin/env bash

# Install the application dependencies
composer install

# Initialize the application
init --env=Development

# Run migrations
yes | php yii migrate
yes | php yii migrate --migrationPath=@yii/rbac/migrations

# Creates default roles and permissions
php yii users/rbac/init

# Creates default admin user (admin/admin)
php yii users/user/create admin admin admin admin@test.com

# Creates default operator user (operator/operator)
php yii users/user/create operator operator operator operator@test.com

# Creates default cities (Москва, Тула)
php yii services/city/create-default