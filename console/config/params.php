<?php
return [
    'adminEmail' => 'admin@example.com',
    'yii.migrations' => [
        '@modules/users/migrations',
        '@modules/services/migrations',
        '@modules/history/migrations',
    ],
];
